
#include <rte_ether.h>
#include <netinet/in.h>
#include "xfsm.h"

#define RTE_LOGTYPE_PARSER RTE_LOGTYPE_USER1
#define GTP_PORT 2152
#define HTTP_PORT 80
#define GTP_HEADER_SIZE 8
#define IPV4_ICMP_PROTO 1

const struct xfsm_hdr_fields xfsm_header_fields[NUM_HDR_FIELDS] = {
        {SRC_MAC,               "SRC_MAC" ,                 6},
        {DST_MAC,               "DST_MAC",                  6},
        {ETH_TYPE,              "ETH_TYPE",                 2},
        {IPv4_SRC,              "IPv4_SRC",                 4},
        {IPv4_DST,              "IPv4_DST",                 4},
        {IPv4_PROTO,            "IPv4_PROTO",               1},
        {IPv4_TOS,              "IPv4_TOS",                 1},
        {ICMP_CODE,             "ICMP_CODE",                1},
        {ICMP_TYPE,             "ICMP_TYPE",                1},
        {ICMP_ID,               "ICMP_ID",                  2},
        {ICMP_SEQ,              "ICMP_SEQ",                 2},
        {ICMP_PAYLOAD,          "ICMP_PAYLOAD",             4},
        {TP_SPORT,              "TCP_SPORT",                2},
        {TP_DPORT,              "TCP_DPORT",                2},
        {TCP_FLAGS,             "TCP_FLAGS",                1},
        {GTP_TEID,              "GTP-U_TEID",               4},
        {HTTP_REQ,              "HTTP_REQ",                 4},
        {INNER_IPv4_SRC,        "INNER_IPv4_SRC",           4},
        {INNER_IPv4_DST,        "INNER_IPv4_DST",           4},
        {INNER_IPv4_PROTO,      "INNER_IPv4_PROTO",         1},
        {INNER_TP_SPORT,        "INNER_TCP_SPORT",          2},
        {INNER_TP_DPORT,        "INNER_TCP_DPORT",          2},
        {INNER_ICMP_CODE,       "INNER_ICMP_CODE",          1},
        {INNER_ICMP_TYPE,       "INNER_ICMP_TYPE",          1},
        {INNER_ICMP_ID,         "INNER_ICMP_ID",            2},
        {INNER_ICMP_SEQ,        "INNER_ICMP_SEQ",           2},
        {INNER_ICMP_PAYLOAD,    "INNER_ICMP_PAYLOAD",       4},
        {XFSM_IN_PORT,          "XFSM_IN_PORT",             1},
        {XFSM_METADATA0,        "METADATA0",                8},
        {XFSM_METADATA1,        "METADATA1",                8},
        {XFSM_METADATA2,        "METADATA2",                8},
        {XFSM_METADATA3,        "METADATA3",                8},
        {XFSM_TIMESTAMP,        "XFSM_TIMESTAMP",           8},
};


static inline uint64_t
be64ptr2h(const uint8_t *byte) {
    uint64_t value = 0;

    for (int i = 0; i < 6; i++)
        value |= (uint64_t) (byte[i] << (5 - i) * 8);
    return value;
}

static inline uint32_t
be32ptr2h(const uint8_t *byte){
    return byte[3] | byte[2] << 8 | byte[1] << 16 | byte[0] << 24;
}

static inline uint16_t
be16ptr2h(const uint8_t *byte){
    return byte[1] | byte[0] << 8;
}

int parse_pkt_header_outside(xfsm_fields_tlv_t *pkt_fields, struct rte_mbuf *packet){
    uint8_t *eth, *ipv4;
    pkt_metadata_t *metadata;

    if (packet == NULL)
        return -1;

    //pkt_fields[XFSM_IN_PORT].value = &xfsm->in_port;
    metadata = (pkt_metadata_t *) packet->userdata;

    if (packet->userdata != NULL){
        pkt_fields[XFSM_METADATA0].value = (uint8_t *) &metadata->metadata[0];
        pkt_fields[XFSM_METADATA1].value = (uint8_t *) &metadata->metadata[1];
        pkt_fields[XFSM_METADATA2].value = (uint8_t *) &metadata->metadata[2];
        pkt_fields[XFSM_METADATA3].value = (uint8_t *) &metadata->metadata[3];
    } else {
        pkt_fields[XFSM_METADATA0].value = NULL;
        pkt_fields[XFSM_METADATA1].value = NULL;
        pkt_fields[XFSM_METADATA2].value = NULL;
        pkt_fields[XFSM_METADATA3].value = NULL;
    }

    eth = rte_pktmbuf_mtod(packet, void *);

    // Ethernet
    pkt_fields[SRC_MAC].value  = &eth[6];
    pkt_fields[DST_MAC].value  = &eth[0];
    pkt_fields[ETH_TYPE].value = &eth[12];

    // IPv4
    ipv4 = eth + ETHER_HDR_LEN;
    pkt_fields[IPv4_DST].value   = &ipv4[16];
    pkt_fields[IPv4_SRC].value   = &ipv4[12];
    pkt_fields[IPv4_PROTO].value = &ipv4[9];
    pkt_fields[IPv4_TOS].value   = &ipv4[1];

    pkt_fields[XFSM_TIMESTAMP].value = (uint8_t *) &packet->timestamp;

    /*printf("Parsing the packet\n");
    for (int i=0; i<NUM_HDR_FIELDS; i++) {
        uint64_t val = 0;

        get_hdr_value(xfsm->pkt_fields[i], &val);
        printf("\t%s: 0x%"PRIX64"\n", xfsm->pkt_fields[i].field->name, val);
    }*/

    return ipv4[9];
}

int parse_pkt_header(xfsm_table_t *xfsm, struct rte_mbuf *packet){
    uint8_t *eth, *ipv4, *tp, *gtp_u, *in_ipv4, *in_tp, *http;
    uint64_t dport = 0, tcp_hdr_len = 0;
    pkt_metadata_t *metadata;


    if (packet == NULL)
        return -1;

    xfsm->pkt_fields[XFSM_IN_PORT].value = &xfsm->in_port;
    metadata = (pkt_metadata_t *) packet->userdata;

    if (packet->userdata != NULL){
        xfsm->pkt_fields[XFSM_METADATA0].value = (uint8_t *) &metadata->metadata[0];
        xfsm->pkt_fields[XFSM_METADATA1].value = (uint8_t *) &metadata->metadata[1];
        xfsm->pkt_fields[XFSM_METADATA2].value = (uint8_t *) &metadata->metadata[2];
        xfsm->pkt_fields[XFSM_METADATA3].value = (uint8_t *) &metadata->metadata[3];
    } else {
        xfsm->pkt_fields[XFSM_METADATA0].value = NULL;
        xfsm->pkt_fields[XFSM_METADATA1].value = NULL;
        xfsm->pkt_fields[XFSM_METADATA2].value = NULL;
        xfsm->pkt_fields[XFSM_METADATA3].value = NULL;
    }

    eth = rte_pktmbuf_mtod(packet, void *);

    // Ethernet
    xfsm->pkt_fields[SRC_MAC].value  = &eth[6];
    xfsm->pkt_fields[DST_MAC].value  = &eth[0];
    xfsm->pkt_fields[ETH_TYPE].value = &eth[12];

    // IPv4
    ipv4 = eth + ETHER_HDR_LEN;
    xfsm->pkt_fields[IPv4_DST].value   = &ipv4[16];
    xfsm->pkt_fields[IPv4_SRC].value   = &ipv4[12];
    xfsm->pkt_fields[IPv4_PROTO].value = &ipv4[9];
    xfsm->pkt_fields[IPv4_TOS].value   = &ipv4[1];

    tp = ipv4 + 20;

    xfsm->pkt_fields[TCP_FLAGS].value = NULL;
    xfsm->pkt_fields[HTTP_REQ].value = NULL;

    if (ipv4[9] == IPPROTO_TCP){
        // TODO: FLAGS
        xfsm->pkt_fields[TP_SPORT].value = &tp[0];
        xfsm->pkt_fields[TP_DPORT].value = &tp[2];

        xfsm->pkt_fields[TCP_FLAGS].value = &tp[13];

        get_hdr_value(xfsm->pkt_fields[TP_DPORT], &dport);

        tcp_hdr_len = (tp[12] & 0xf0u) >> 2u;

        if (dport == HTTP_PORT && packet->data_len > 66) {
            http = &tp[tcp_hdr_len];

            xfsm->pkt_fields[HTTP_REQ].value = &http[0];
        }
    } else if (ipv4[9] == IPV4_ICMP_PROTO) {
        xfsm->pkt_fields[ICMP_TYPE].value    = &tp[0];
        xfsm->pkt_fields[ICMP_CODE].value    = &tp[1];
        xfsm->pkt_fields[ICMP_ID].value      = &tp[4];
        xfsm->pkt_fields[ICMP_SEQ].value     = &tp[6];
        xfsm->pkt_fields[ICMP_PAYLOAD].value = &tp[8];

        // set to null other transport protocols
        xfsm->pkt_fields[TP_SPORT].value = NULL;
        xfsm->pkt_fields[TP_DPORT].value = NULL;
    } else if (ipv4[9] == IPPROTO_UDP) {
        xfsm->pkt_fields[TP_SPORT].value = &tp[0];
        xfsm->pkt_fields[TP_DPORT].value = &tp[2];

        get_hdr_value(xfsm->pkt_fields[TP_DPORT], &dport);

        if (dport == GTP_PORT) {
            gtp_u = &tp[8];
            xfsm->pkt_fields[GTP_TEID].value = &gtp_u[4];

            // Inner IPv4
            in_ipv4 = gtp_u + GTP_HEADER_SIZE;
            xfsm->pkt_fields[INNER_IPv4_DST].value   = &in_ipv4[16];
            xfsm->pkt_fields[INNER_IPv4_SRC].value   = &in_ipv4[12];
            xfsm->pkt_fields[INNER_IPv4_PROTO].value = &in_ipv4[9];

            in_tp = in_ipv4 + 20;

            if (in_ipv4[9] == IPPROTO_TCP || in_ipv4[9] == IPPROTO_UDP) {
                // TODO: FLAGS
                xfsm->pkt_fields[INNER_TP_SPORT].value = &in_tp[0];
                xfsm->pkt_fields[INNER_TP_DPORT].value = &in_tp[2];
            } else if (in_ipv4[9] == IPPROTO_ICMP) {
                xfsm->pkt_fields[INNER_ICMP_TYPE].value    = &in_tp[0];
                xfsm->pkt_fields[INNER_ICMP_CODE].value    = &in_tp[1];
                xfsm->pkt_fields[INNER_ICMP_ID].value      = &in_tp[4];
                xfsm->pkt_fields[INNER_ICMP_SEQ].value     = &in_tp[6];
                xfsm->pkt_fields[INNER_ICMP_PAYLOAD].value = &in_tp[8];

                xfsm->pkt_fields[INNER_TP_SPORT].value = NULL;
                xfsm->pkt_fields[INNER_TP_DPORT].value = NULL;
            }
        } else {
            xfsm->pkt_fields[GTP_TEID].value            = NULL;
            xfsm->pkt_fields[INNER_IPv4_DST].value      = NULL;
            xfsm->pkt_fields[INNER_IPv4_SRC].value      = NULL;
            xfsm->pkt_fields[INNER_IPv4_PROTO].value    = NULL;
            xfsm->pkt_fields[INNER_TP_SPORT].value      = NULL;
            xfsm->pkt_fields[INNER_TP_DPORT].value      = NULL;
        }
    }

    xfsm->pkt_fields[XFSM_TIMESTAMP].value = (uint8_t *) &packet->timestamp;

    /*printf("Parsing the packet\n");
    for (int i=0; i<NUM_HDR_FIELDS; i++) {
        uint64_t val = 0;

        get_hdr_value(xfsm->pkt_fields[i], &val);
        printf("\t%s: 0x%"PRIX64"\n", xfsm->pkt_fields[i].field->name, val);
    }*/

    return ipv4[9];
}

int get_hdr_value(xfsm_fields_tlv_t field, uint64_t *value) {

    if (field.value == NULL)
        return -1;

    uint8_t *byte = field.value;
    switch (field.field->len) {
        case 8:
            if (field.field->type == XFSM_TIMESTAMP ||
                    field.field->type == XFSM_METADATA0 ||
                    field.field->type == XFSM_METADATA1 ||
                    field.field->type == XFSM_METADATA2 ||
                    field.field->type == XFSM_METADATA3)
                *value = *((uint64_t *) byte);
            break;
        case 6:
            *value = be64ptr2h(byte);
            break;
        case 4:
            *value = be32ptr2h(byte);
            break;
        case 2:
            *value = be16ptr2h(byte);
            break;
        case 1:
            *value = byte[0];
            break;
        default:
            break;
    }

    return 0;

}
