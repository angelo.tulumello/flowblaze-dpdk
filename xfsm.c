//
// Created by angelo on 13/12/18.
//

#include <stdlib.h>
#include <stdio.h>
#include <assert.h>
#include <inttypes.h>
#include <rte_acl_osdep.h>
#include <rte_hash.h>
#include <rte_jhash.h>
#include <rte_mempool.h>
#include <rte_ethdev.h>
#include <rte_ip.h>
#include <rte_udp.h>
#include <rte_hash_crc.h>
#include <rte_mbuf.h>
#include "xfsm.h"

const char *xfsm_action_names[] = {
    "XFSM_UPDATE",
    "XFSM_SENDPKT",
    "XFSM_SETFIELD",
    "XFSM_DESTROY",
    "XFSM_ENCAP_GTP",
    "XFSM_DECAP_GTP",
    "XFMS_GENPKT"
};

#define RTE_LOGTYPE_XFSM RTE_LOGTYPE_USER1

static void
init_instance(xfsm_instance_t * inst, xfsm_table_t *table){
    inst->destroy = false;
    memset(inst->results, XFSM_TRUE, sizeof(xfsm_results_t) * MAX_CONDITIONS);
    memset(inst->flow_key.flds, 0, sizeof(flow_key_t));
    inst->table = table;
    memset(inst->dbg_regs, 0, sizeof(dbg_reg_t) * MAX_DEBUG_REGISTERS);
    memset(inst->registers, 0, sizeof(int64_t) * NUM_REGISTERS);
    inst->current_state = table->default_state;
    inst->id = 0;
}

static bool
search_key(xfsm_search_key_t *key1, key_node_t *list) {
    key_node_t *keyNode;
    keyNode = list;
    RTE_LOG(INFO, XFSM, "Searching key\n");

    while (keyNode != NULL) {
        xfsm_search_key_t *key2;
        key2 = &keyNode->key;
        if (memcmp(key1, key2, sizeof(xfsm_search_key_t)) == 0) {
            RTE_LOG(INFO, XFSM, "key found: %u\n", key1->state);
            return true;
        }
        keyNode = keyNode->next;
    }

    return false;
}

static int
insert_key(xfsm_search_key_t *key, key_node_t *list) {
    key_node_t *keyNode;
    keyNode = list;

    // in case it's the first key
    /*if (key->state == STATE_NULL) {
        memcpy(&keyNode->key, key, sizeof(xfsm_search_key_t));
        RTE_LOG(INFO, XFSM, "inserting first key: %u\n", key->state);
        return 0;
    }*/
    while (keyNode != NULL) {
        if (keyNode->next == NULL){
            keyNode->next = malloc(sizeof(key_node_t));
            keyNode = keyNode->next;
            memcpy(&keyNode->key, key, sizeof(xfsm_search_key_t));
            keyNode->size = 0;
            RTE_LOG(INFO, XFSM, "inserting key: %u\n", key->state);
            keyNode->next=NULL;
            return 0;
        } else
            keyNode = keyNode->next;
    }
    return 0;
}

static int
insert_entry(int id, key_node_t *list, xfsm_search_key_t *key) {
    key_node_t *keyNode = list;

    while (keyNode != NULL) {
        xfsm_search_key_t *key2;
        key2 = &keyNode->key;
        if (memcmp(key, key2, sizeof(xfsm_search_key_t)) == 0) {
            keyNode->entries[keyNode->size] = id;
            keyNode->size++;
            RTE_LOG(INFO, XFSM, "inserting entry: id=%u\n", id);
            return 0;
        }
        keyNode = keyNode->next;
    }

    return -1;
}

/*
 * Function to create the description of a state table
 */
xfsm_table_t *
xfsm_table_create(xfsm_table_entry_t **entries, uint8_t table_size, uint8_t initial_state,
                  xfsm_condition_t *conditions, uint8_t conditions_number, lookup_key_t *lookup_key, uint8_t table_id)
{
    key_node_t *list = malloc(sizeof(key_node_t));
    key_node_t *head = list;
    xfsm_search_key_t *key = malloc(sizeof(xfsm_search_key_t));
    uint8_t key_num = 0;
    struct rte_hash_parameters instances_hash_params = {0};
#ifdef  CACHE_ENABLE
    struct rte_hash_parameters cache_hash_params = {0};
    char cache_hash_name[32];
    snprintf(cache_hash_name, sizeof(cache_hash_name), "cache_hash_%02d-%02d", table_id, rte_lcore_id());
#endif
    xfsm_table_t *xfsm;
    char xfsm_name[32];
    char entries_db_name[32];
    char entries_hash_name[32];
    char instances_hash_name[32];
    char instances_pool_name[32];
    char pkttmp_pool_name[32];

    snprintf(xfsm_name, sizeof(xfsm_name), "xfsm_table_%02d-%02d", table_id, rte_lcore_id());
    snprintf(entries_db_name, sizeof(entries_db_name), "entries_db_%02d-%02d", table_id, rte_lcore_id());
    snprintf(entries_hash_name, sizeof(entries_hash_name), "entries_hash_%02d-%02d", table_id, rte_lcore_id());
    snprintf(instances_hash_name, sizeof(instances_hash_name), "instances_hash_%02d-%02d", table_id, rte_lcore_id());
    snprintf(instances_pool_name, sizeof(instances_pool_name), "instances_pool_%02d-%02d", table_id, rte_lcore_id());
    snprintf(pkttmp_pool_name, sizeof(pkttmp_pool_name), "pkttmp_pool_%02d-%02d", table_id, rte_lcore_id());

    memset(list->entries, 0, sizeof(int)*MAX_ENTRIES_FOREACH_STATE);
    list->key.state = 0;
    list->size = 0;
    list->next = NULL;

    // Populate keys_list
    for (int i=0; i<table_size; i++) {
        // here I have to setup the right tuple value
            // setup_key()
        key->state = entries[i]->current_state;

        if (!search_key(key, list)){
            insert_key(key, list);
            insert_entry(i, list, key);
            key_num++;
        } else {
            insert_entry(i, list, key);
        }
    }

    key_node_t *ptr = head;

    // Xfsm initialization
    xfsm = rte_zmalloc(xfsm_name, sizeof(xfsm_table_t), CACHE_LINE_SIZE);

    // create packet template pool
    xfsm->pkttmp_pool = rte_pktmbuf_pool_create(pkttmp_pool_name, PKTTMP_POOL_SIZE, CACHE_LINE_SIZE/16, 0,
                                                RTE_MBUF_DEFAULT_BUF_SIZE, rte_socket_id());

    if (xfsm->pkttmp_pool == NULL)
        rte_exit(EXIT_FAILURE, "Cannot create packet template pool\n");


    // Allocate the entries db
    xfsm->state_entries_pool = rte_zmalloc(entries_db_name, 255 * sizeof(hmap_entry_t), CACHE_LINE_SIZE);

    int k = 0;
    while (ptr != NULL) {
        hmap_entry_t state_entry = {0};

        // for each node on the list we create an array of entries
        for (uint32_t i=0; i<ptr->size; i++) {
            RTE_LOG(INFO, XFSM, "Size = %d, first ptr is %p, second is %p\n", ptr->size, &state_entry.table_entries[i],
                   entries[ptr->entries[i]]);
            memcpy(&state_entry.table_entries[i], entries[ptr->entries[i]], sizeof(xfsm_table_entry_t));
        }

        memcpy(&state_entry.key, &ptr->key, sizeof(xfsm_search_key_t));
        state_entry.size = ptr->size;

        RTE_LOG(INFO, XFSM, "Created new state table entry: state=%d\n", state_entry.key.state);
        rte_memcpy(&xfsm->state_entries_pool[state_entry.key.state], &state_entry, sizeof(hmap_entry_t));

        ptr = ptr->next;
        k++;
    }

    // Initialize instances hashmap
    instances_hash_params.name = instances_hash_name;
    instances_hash_params.entries = MAX_INSTANCES;          // number of maximum instances
    instances_hash_params.key_len = (uint32_t) lookup_key->size * 4;     // size of the key
    instances_hash_params.hash_func = rte_jhash;
    instances_hash_params.hash_func_init_val = 0;
    instances_hash_params.socket_id = rte_socket_id();
    instances_hash_params.reserved = 0;
    instances_hash_params.extra_flag = 0;

    xfsm->instances_hmap = rte_hash_create(&instances_hash_params);

    if (xfsm->instances_hmap == NULL)
        printf("Cannot create instances hmap\n");

    // allocate all the instances
    xfsm->instances_pool = rte_zmalloc(instances_pool_name,
                                       MAX_INSTANCES * sizeof(xfsm_instance_t), CACHE_LINE_SIZE/16);

    if (xfsm->instances_pool == NULL) {
        printf("Cannot allocate instances pool\n");
    }

    for (int i=0; i<MAX_INSTANCES; i++)
        init_instance(&xfsm->instances_pool[i], xfsm);

#ifdef CACHE_ENABLE
    /*
     * Create the flow cache
     */
    cache_hash_params.name = cache_hash_name;
    cache_hash_params.entries = (uint32_t) (MAX_INSTANCES + 1 * conditions_number) - 1;
    cache_hash_params.key_len = sizeof(cache_key_t);
    cache_hash_params.hash_func = rte_jhash;
    cache_hash_params.hash_func_init_val = 0;
    cache_hash_params.socket_id = rte_socket_id();
    cache_hash_params.reserved = 0;
    cache_hash_params.extra_flag = 0;

    xfsm->cache = rte_hash_create(&cache_hash_params);

#endif


    // final configurations
    memcpy(xfsm->conditions, conditions, sizeof(xfsm_condition_t) * MAX_CONDITIONS);
    xfsm->nb_conditions = conditions_number;
    memcpy(&xfsm->lookup_key, lookup_key, sizeof(lookup_key_t));
    xfsm->default_state = initial_state;
    memset(xfsm->global_registers, 0, sizeof(int64_t) * NUM_GLOBAL_REGISTERS);
    xfsm->in_port = INVALID_PORT;
    xfsm->nb_instances = 0;
    xfsm->table_id = table_id;
    xfsm->stateful = true;

    xfsm->pkt_fields = rte_zmalloc_socket("fields", sizeof(xfsm_fields_tlv_t)*NUM_HDR_FIELDS, CACHE_LINE_SIZE, rte_socket_id());

    for (int i=0; i<NUM_HDR_FIELDS; i++){
        xfsm->pkt_fields[i].field = &xfsm_header_fields[i];
        xfsm->pkt_fields[i].value = NULL;
    }

    RTE_LOG(INFO, XFSM, "XFSM Table successfully created\n");

    return xfsm;
}

struct rte_mbuf*
create_pkttmp(xfsm_table_t *xfsm, uint8_t *buf, uint16_t len, uint8_t id) {
    struct rte_mbuf *pkttmp;
    char *data;

    pkttmp = rte_pktmbuf_alloc(xfsm->pkttmp_pool);
    if (pkttmp == NULL)
        rte_exit(EXIT_FAILURE, "Cannot allocate a packet to the mempool %s\n", xfsm->pkttmp_pool->name);

    if (id < PKTTMP_POOL_SIZE)
        xfsm->pkttmps[id] = pkttmp;
    else {
        RTE_LOG(ERR, XFSM, "Id overflowed packet template pool size: %d\n", PKTTMP_POOL_SIZE);
        return NULL;
    }

    data = rte_pktmbuf_append(pkttmp, len);

    rte_memcpy_aligned(data, buf, len);

    return pkttmp;
}

int
destroy_instance(xfsm_instance_t *inst) {
    int32_t ret = 0;

    ret = rte_hash_del_key(inst->table->instances_hmap, &inst->flow_key);

    if (ret < 0) {
        RTE_LOG(ERR, XFSM, "Error: cannot delete key from hasmap\n");
    } else {
        init_instance(inst, inst->table);
    }
    return ret;
}

xfsm_instance_t *
insert_instance(xfsm_table_t *xfsm, flow_key_t *fk) {
    int32_t ret = 0;
    xfsm_instance_t *instance = NULL;
    if (xfsm->nb_instances <= MAX_INSTANCES) {
        ret = rte_hash_add_key(xfsm->instances_hmap, fk);
        if (ret >= 0) {
            RTE_LOG(INFO, XFSM, "\t\tInstance inserted: %d\n", ret);
            instance = &xfsm->instances_pool[ret];
            rte_memcpy(&instance->flow_key, fk, sizeof(flow_key_t));
            xfsm->nb_instances++;
            return instance;
        } else {
            printf("\t\terrors in insering instance: %d\n", ret);
        }
    } else {
        printf("\t\tInstances overflowed\n");
        rte_exit(EXIT_FAILURE, "Exit instances overflowed");
    }
    return NULL;
}

int32_t
insert_instance_pos(xfsm_table_t *xfsm, flow_key_t *fk) {
    int32_t ret = 0;
    xfsm_instance_t *instance = NULL;
    if (xfsm->nb_instances <= MAX_INSTANCES) {
        ret = rte_hash_add_key(xfsm->instances_hmap, fk);
        if (ret >= 0) {
            RTE_LOG(INFO, XFSM, "\t\tInstance inserted: %d\n", ret);
            instance = &xfsm->instances_pool[ret];
            rte_memcpy_aligned(&instance->flow_key, fk, sizeof(flow_key_t));
            xfsm->nb_instances++;
            return ret;
        } else {
            RTE_LOG(INFO, XFSM, "\t\terrors in insering instance: %d\n", ret);
        }
    } else {
        RTE_LOG(ERR, XFSM, "\t\tInstances overflowed\n");
    }
    return -ENOENT;
}

void
lookup_instance_bulk(xfsm_table_t *xfsm, flow_key_t **flow_keys, uint8_t nb_keys, int32_t *pos) {
    int32_t ret = 0;

    ret = rte_hash_lookup_bulk(xfsm->instances_hmap, (const void **) flow_keys, nb_keys, pos);

    if (unlikely(ret != 0))
        rte_exit(EXIT_FAILURE, "Error on hash lookup bulk\n");

    for (int i=0; i<nb_keys; i++) {
        if (unlikely(pos[i] == -ENOENT))
            pos[i] = insert_instance_pos(xfsm, flow_keys[i]);
    }
}

xfsm_instance_t *
lookup_instance(xfsm_table_t *xfsm, flow_key_t *flow_key) {
    xfsm_instance_t *instance = NULL;
    int32_t ret = 0;

    ret = rte_hash_lookup(xfsm->instances_hmap, flow_key);
    //ret = rte_hash_lookup_with_hash(xfsm->instances_hmap, flow_key, xfsm->curr_pkt->hash.rss);

    if (ret >= 0) {
        RTE_LOG(INFO, XFSM, "Instance found!\n");
        instance = &xfsm->instances_pool[ret];
    } else if (unlikely(ret == -ENOENT)) {
        RTE_LOG(INFO, XFSM, "\n\t\tInstance not found\n");
        instance = insert_instance(xfsm, flow_key);
        if (instance == NULL)
            printf("error in instance insertion\n");
    } else if (unlikely(ret == -EINVAL))
        printf("\n\t\t error in lookup");

    return instance;
}

xfsm_table_entry_t *
cache_lookup(xfsm_table_t *xfsm, cache_key_t *key) {
    int32_t ret = 0;
    xfsm_table_entry_t *match = NULL;

    ret = rte_hash_lookup_data(xfsm->cache, key, (void **) &match);

    if (ret == -ENOENT) {
        return NULL;
    } else {
        return match;
    }
}

int
cache_insert(xfsm_table_t *xfsm, cache_key_t *key, xfsm_table_entry_t *entry) {
    int32_t ret = 0;

    //ret = rte_hash_add_key(xfsm->cache, key);
    ret = rte_hash_add_key_data(xfsm->cache, key, (void *) entry);

    if (ret==0)
        return 0;
    else {
        RTE_LOG(ERR, XFSM, "Cannot insert cache entry. Error code: %d\n", ret);
        return ret;
    }
}

int
xfsm_table_lookup_id(xfsm_instance_t *instance) {
    hmap_entry_t *state_entry;
    xfsm_table_entry_t *match = NULL;

    RTE_LOG(INFO, XFSM, "\n\nLookup. KEY: state=%d\n\n", instance->current_state);

    state_entry = &instance->table->state_entries_pool[instance->current_state];

    if (state_entry){
        if (instance->table->nb_conditions == 0)
            return state_entry->table_entries[0].id;

        // while loop scanning all hash table entries
        for (int index=0; index < state_entry->size; index++) {
            match = &state_entry->table_entries[index];

            // found entry; now look for matching conditions
            for (int i=0; i<instance->table->nb_conditions; i++) {
                // don't care: go on
                if (match->results[i] == XFSM_DONT_CARE) {
                    if (i == instance->table->nb_conditions - 1)
                        return index;
                    else
                        continue;
                }
                    // there is something
                else {
                    // if I match a condition, continue
                    if (match->results[i] == instance->results[i]) {
                        // if even the last condition matches, return the result
                        if (i == instance->table->nb_conditions - 1)
                            return index;
                        else
                            continue;
                    } else
                        break;
                }
            }

        }

        RTE_LOG(INFO, XFSM, "Match not found\n");
        return -1;
    } else {
        RTE_LOG(INFO, XFSM, "No matching state entry found\n");
        return -1;
    }
}

xfsm_table_entry_t*
xfsm_table_lookup(xfsm_instance_t *instance) {
    hmap_entry_t *state_entry;
    xfsm_table_entry_t *match = NULL;

    RTE_LOG(INFO, XFSM, "\n\nLookup. KEY: state=%d\n\n", instance->current_state);

    state_entry = &instance->table->state_entries_pool[instance->current_state];

    if (state_entry){
        if (instance->table->nb_conditions == 0)
            return &state_entry->table_entries[0];

        // while loop scanning all hash table entries
        for (int index=0; index < state_entry->size; index++) {
            match = &state_entry->table_entries[index];

            // found entry; now look for matching conditions
            for (int i=0; i<instance->table->nb_conditions; i++) {
                // don't care: go on
                if (match->results[i] == XFSM_DONT_CARE) {
                    if (i == instance->table->nb_conditions - 1)
                        return match;
                    else
                        continue;
                }
                    // there is something
                else {
                    // if I match a condition, continue
                    if (match->results[i] == instance->results[i]) {
                        // if even the last condition matches, return the result
                        if (i == instance->table->nb_conditions - 1)
                            return match;
                        else
                            continue;
                    } else
                        break;
                }
            }

        }

        RTE_LOG(INFO, XFSM, "Match not found\n");
        return NULL;
    } else {
        RTE_LOG(INFO, XFSM, "No matching state entry found\n");
        return NULL;
    }
}

/**
 * Evaluate a single condition
 * @param cond          structure describing the condition
 * @param reg[in,out]   array of xfsm registers passed by reference
 * @return
 */
static xfsm_results_t
evaluate_condition(xfsm_condition_t cond, xfsm_instance_t *inst) {
    xfsm_condition_types_t opcode = cond.condition_type;
    bool result = false;
    int64_t param1 = 0, param2 = 0;
    int64_t *reg = inst->registers;
    int64_t *gr = inst->table->global_registers;

    switch (cond.type1){
        case XFSM_REGISTER:
            param1 = reg[cond.value1];
            break;
        case XFSM_GLOBAL_REGISTER:
            param1 = gr[cond.value1];
            break;
        case XFSM_CONSTANT:
            param1 = cond.value1;
            break;
        case XFSM_HDR_FIELD:
            if (get_hdr_value(inst->table->pkt_fields[cond.value1], (uint64_t *) &param1) < 0)
                RTE_LOG(ERR, XFSM, "Packet field not found\n");
            break;
        default:
            //RTE_LOG(INFO, XFSM, )("Invalid register 1 type\n");
            break;
    }

    switch (cond.type2){
        case XFSM_REGISTER:
            param2 = reg[cond.value2];
            break;
        case XFSM_GLOBAL_REGISTER:
            param2 = gr[cond.value2];
            break;
        case XFSM_CONSTANT:
            param2 = cond.value2;
            break;
        case XFSM_HDR_FIELD:
            if (get_hdr_value(inst->table->pkt_fields[cond.value2], (uint64_t *) &param2) < 0)
                RTE_LOG(ERR, XFSM, "Packet field not found\n");
            break;
        default:
            //RTE_LOG(INFO, XFSM, )("Invalid register 2 type\n");
            break;
    }


    switch (opcode) {
        case XFSM_LESS:
        {
            result = param1 < param2;
            break;
        }
        case XFSM_LESS_EQ:
        {
            result = param1 <= param2;
            break;
        }
        case XFSM_GREATER:
        {
            result = param1 > param2;
            break;
        }
        case XFSM_GREATER_EQ:
        {
            result = param1 >= param2;
            break;
        }
        case XFSM_EQUAL:
        {
            result = param1 == param2;
            break;
        }
        case XFSM_NOT_EQUAL:
        {
            result = param1 != param2;
            break;
        }
    }

    if (result)
        return XFSM_TRUE;
    else
        return XFSM_FALSE;
}

void
xfsm_evaluate_conditions(xfsm_instance_t *xfsm) {
    int i = 0;

    /*for (i=0; i<NUM_REGISTERS/sizeof(uint64_t); i+=CACHE_LINE_SIZE) {
        rte_prefetch0(xfsm->registers + i);
        rte_prefetch0(xfsm->table->global_registers+i);
    }*/

    for (i=0; i<xfsm->table->nb_conditions; i++) {
        xfsm->results[i] = evaluate_condition(xfsm->table->conditions[i], xfsm);
        RTE_LOG(INFO, XFSM, "Evaluating condition %d, result=%s\n",
                i, xfsm->results[i] == XFSM_FALSE ? "False" : "True");
    }
}


/**
 * Action update: calculate operation and update registers
 * @param reg1              first register of the operation
 * @param reg2              first register of the operation. Could be XFSM_NULL
 * @param ext_param         if reg2 = XFSM_NULL param2 would be the second parameter
 * @param opcode            operation type
 * @param output            output register
 * @param xfsm_regs[out,in] reference to xfsm registers array
 */
static inline void
update(int64_t reg1, int64_t reg2, xfsm_opcodes_t opcode,
       uint8_t output, xfsm_value_type_t out_type, int64_t *xfsm_regs, int64_t *global_regs) {
    int64_t result = 0;

    switch(opcode)
    {
        case(XFSM_SUM):
            result = reg1 + reg2;
            break;
        case(XFSM_MINUS):
            result = reg1 - reg2;
            break;
        case(XFSM_MULTIPLY):
            result = reg1 * reg2;
            break;
        case(XFSM_DIVIDE):
        {
            if (reg2==0)
                result = reg1;
            else
                result = reg1 / reg2;
        }
            break;
        case (XFSM_MODULO):
            result = reg1 % reg2;
            break;
        case(XFSM_MAX):
            result = reg1 > reg2 ? reg1 : reg2;
            break;
        case(XFSM_MIN):
            result = reg1 < reg2 ? reg1 : reg2;
            break;
        default:
            break; // Some warning log
    }
    if (out_type == XFSM_REGISTER)
        xfsm_regs[output] = result;
    else
        global_regs[output] = result;
}


/*void xfsm_pack_actions(xfsm_table_entry_t *entry, xfsm_instance_t *xfsm) {
    xfsm_packed_actions_t pact[entry->nb_actions] = {0};

    RTE_LOG(INFO, XFSM, "Entering execute actions\n");

    for (int i=0; i<entry->nb_actions; i++) {
        xfsm_action_t *act = &entry->actions[i];
        RTE_LOG(INFO, XFSM, "Processing action %d: type=%s\n", i, xfsm_action_names[act->type]);
        pact->type = act->type;

        switch (act->type1) {
            case XFSM_REGISTER:
                pact[i].reg1 = &xfsm->registers[act->value1];
                break;
            case XFSM_GLOBAL_REGISTER:
                pact[i].reg1 = &xfsm->table->global_registers[act->value1];
                break;
            case XFSM_CONSTANT:
                pact[i].imm1 = act->value1;
                pact[i].reg1 = NULL;
                break;
            case XFSM_HDR_FIELD:
                if (act->type == XFSM_SETFIELD)
                    pact[i].imm1 = act->value1;
                else {
                    pact[i].reg1 = xfsm->table->pkt_fields[act->value1].value;
                }
                break;
            default:
                if (act->type == XFSM_DESTROY)
                    break;
                RTE_LOG(INFO, XFSM, "Invalid field 1 type\n");
                break;
        }

        switch (act->type2) {
            case XFSM_REGISTER:
                pact[i].reg2 = &xfsm->registers[act->value2];
                break;
            case XFSM_GLOBAL_REGISTER:
                pact[i].reg2 = &xfsm->table->global_registers[act->value2];
                break;
            case XFSM_CONSTANT:
                pact[i].imm2 = act->value2;
                pact[i].reg2 = NULL;
                break;
            case XFSM_HDR_FIELD:
                pact[i].reg2 = xfsm->table->pkt_fields[act->value2].value;
                break;
            default:
                if (act->type == XFSM_DESTROY)
                    break;
                RTE_LOG(INFO, XFSM, "Invalid field 2 type\n");
                break;
        }

        switch (act->type3) {
            case XFSM_REGISTER:
                pact[i].reg3 = &xfsm->registers[act->value3];
                break;
            case XFSM_GLOBAL_REGISTER:
                pact[i].reg3 = &xfsm->table->global_registers[act->value3];
                break;
            case XFSM_CONSTANT:
                pact[i].imm3 = act->value3;
                break;
            case XFSM_HDR_FIELD:
                pact[i].reg3 = xfsm->table->pkt_fields[act->value3].value;
                break;
            default:
                if (act->type == XFSM_DESTROY)
                    break;
                RTE_LOG(INFO, XFSM, "Invalid field 3 type\n");
                break;
        }

        if (act->out_type == XFSM_GLOBAL_REGISTER)
            pact[i].output = &xfsm->table->global_registers[act->output];
        else
            pact[i].output = &xfsm->registers[act->output];
    }
}*/

/*
int
xfsm_execute_actions_id(uint8_t id, xfsm_instance_t *xfsm, struct rte_eth_dev_tx_buffer *tx_buffer) {
    int64_t *reg1 = 0, *reg2 = 0, *reg3 = 0;
    int i;

    RTE_LOG(INFO, XFSM, "Entering execute actions\n");

    for (i=0; i<NUM_REGISTERS/sizeof(uint64_t); i+=CACHE_LINE_SIZE) {
        rte_prefetch0(xfsm->registers + i);
        rte_prefetch0(xfsm->table->global_registers+i);
    }

    for (i=0; i<xfsm->actions[id].nb_actions; i++){
        // todo prefetch

        xfsm_packed_actions_t *act = &xfsm->actions[id].actions[i];
        RTE_LOG(INFO, XFSM, "Processing action %d: type=%s\n", i, xfsm_action_names[act->type]);

        switch(act->type) {
            case XFSM_UPDATE:
            {
                update(reg1, reg2, act->opcode, act->output, act->out_type,
                       xfsm->registers, xfsm->table->global_registers);
                break;
            }
                //                reg1    const|reg2
                // send_packet(packet_ref, out_port);
            case XFSM_SENDPKT:
            {
                reg2 = act->value2;

                int ret = 0;
                if (unlikely(reg2 == 0)) {
                    RTE_LOG(ERR, XFSM, "out port is 0|\n");
                    break;
                }

                xfsm->table->curr_pkt->l2_len = ETHER_HDR_LEN;
                xfsm->table->curr_pkt->l3_len = IPv4_HDR_LEN;
                xfsm->table->curr_pkt->userdata = NULL;

                // set ipv4 checksum to 0
                memset(xfsm->table->pkt_fields[IPv4_TOS].value + 9, 0, 2);
                xfsm->table->curr_pkt->ol_flags |= PKT_TX_IPV4 | PKT_TX_IP_CKSUM;

                ret = rte_eth_tx_buffer((uint16_t) (reg2-1), 0, &tx_buffer[reg2-1], xfsm->table->curr_pkt);
                RTE_LOG(INFO, XFSM, "out_port: %d, return code: %d\n", (int) reg2-1, ret);
                break;
            }
                // set_field(field_name, pkt_ref, value)
                //              reg1      const    reg3
            case XFSM_SETFIELD:
            {
                uint64_t field_name, value;

                field_name = (uint64_t) reg1;
                value = (uint64_t) reg3;

                switch (xfsm->table->pkt_fields[field_name].field->len){
                    case 8:
                        value = (uint64_t) reg3;
                        break;
                    case 6:
                        value <<= 16;
                        value = rte_cpu_to_be_64(value);
                        break;
                    case 4:
                        value = rte_cpu_to_be_32( (uint32_t) value);
                        break;
                    case 2:
                        value = rte_cpu_to_be_16( (uint16_t) value);
                        break;
                    default:
                        break;
                }

                if (xfsm->table->pkt_fields[field_name].value != NULL) {
                    // value of packet fields are pointers to the packet field
                    // this memcpy copies the value directly in the packet header

                    rte_memcpy(xfsm->table->pkt_fields[field_name].value, &value,
                               xfsm->table->pkt_fields[field_name].field->len);

                    RTE_LOG(INFO, XFSM, "Executing set field: field: %s, value: %lu\n",
                            xfsm->table->pkt_fields[field_name].field->name, value);
                } else
                    RTE_LOG(INFO, XFSM, "Field not found in packet header... doing nothing\n");

                break;
            }
            case XFSM_DESTROY:
            {
                // good bye my darling
                xfsm->destroy = true;
                break;
            }
            case XFSM_ENCAP_GTP:
            {
                uint8_t *pkt_data = rte_pktmbuf_mtod(xfsm->table->curr_pkt, uint8_t *);
                uint8_t off = 0;
                if (rte_pktmbuf_headroom(xfsm->table->curr_pkt) >= IPv4_HDR_LEN + UDP_HDR_LEN + GTP_HDR_LEN) {
                    rte_memcpy(pkt_data-(IPv4_HDR_LEN+UDP_HDR_LEN+GTP_HDR_LEN), pkt_data, ETHER_HDR_LEN);

                    xfsm->table->curr_pkt->data_len += IPv4_HDR_LEN + UDP_HDR_LEN + GTP_HDR_LEN;
                    xfsm->table->curr_pkt->data_off -= IPv4_HDR_LEN + UDP_HDR_LEN + GTP_HDR_LEN;
                } else if (rte_pktmbuf_tailroom(xfsm->table->curr_pkt) >= IPv4_HDR_LEN + UDP_HDR_LEN + GTP_HDR_LEN) {
                    rte_memcpy(pkt_data + (ETHER_HDR_LEN+IPv4_HDR_LEN+UDP_HDR_LEN+GTP_HDR_LEN),
                               pkt_data + ETHER_HDR_LEN,
                               (size_t ) rte_pktmbuf_data_len(xfsm->table->curr_pkt) - ETHER_HDR_LEN);

                    xfsm->table->curr_pkt->data_len += IPv4_HDR_LEN + UDP_HDR_LEN + GTP_HDR_LEN;
                } else {
                    rte_exit(EXIT_FAILURE, "Error: no space left on pktmbuf structure\n");
                }

                // populate the outer headers
                // IPv4
                pkt_data = rte_pktmbuf_mtod(xfsm->table->curr_pkt, uint8_t *);
                off += ETHER_HDR_LEN;
                struct ipv4_hdr *oip = (struct ipv4_hdr *) (pkt_data + off);
                oip->version_ihl = 0x45;
                oip->type_of_service = 0;
                oip->fragment_offset = 0;
                oip->hdr_checksum = 0;
                oip->packet_id = 1;
                oip->next_proto_id = 0x11;
                oip->time_to_live = 64;
                oip->total_length = (uint16_t ) rte_cpu_to_be_16(rte_pktmbuf_data_len(xfsm->table->curr_pkt)
                                                                 - ETHER_HDR_LEN);
                oip->dst_addr = 0xaabbccdd;
                oip->src_addr = 0xddccbbaa;

                off += IPv4_HDR_LEN;
                struct udp_hdr *udp = (struct udp_hdr *) (pkt_data + off);
                udp->src_port = rte_cpu_to_be_16(54545);
                udp->dst_port = rte_cpu_to_be_16(GTP_UDP_PORT);
                udp->dgram_len = (uint16_t) rte_cpu_to_be_16(rte_pktmbuf_data_len(xfsm->table->curr_pkt)
                                                             - ETHER_HDR_LEN - IPv4_HDR_LEN);
                udp->dgram_cksum = 0;

                off += UDP_HDR_LEN;
                struct gtp_u_header *gtp = (struct gtp_u_header *) (pkt_data + off);
                gtp->flags = 0x30;
                gtp->msg_type = 0xff;
                gtp->len = (uint16_t ) rte_cpu_to_be_16(rte_pktmbuf_data_len(xfsm->table->curr_pkt)
                                                        - ETHER_HDR_LEN - IPv4_HDR_LEN - UDP_HDR_LEN - GTP_HDR_LEN);
                gtp->teid = 0x10101010;

                parse_pkt_header(xfsm->table, xfsm->table->curr_pkt);

                break;
            }
            case XFSM_DECAP_GTP:
            {
                uint8_t *pkt_data = rte_pktmbuf_mtod(xfsm->table->curr_pkt, uint8_t *);

                rte_memcpy(pkt_data + IPv4_HDR_LEN + UDP_HDR_LEN + GTP_HDR_LEN, pkt_data, ETHER_HDR_LEN);
                xfsm->table->curr_pkt->data_off += IPv4_HDR_LEN + UDP_HDR_LEN + GTP_HDR_LEN;
                xfsm->table->curr_pkt->data_len -= IPv4_HDR_LEN + UDP_HDR_LEN + GTP_HDR_LEN;

                break;
            }
            case XFMS_GENPKT:
            {
                uint8_t ref = (uint8_t) reg1;
                pkt_metadata_t *metadata;

                if (xfsm->table->pkttmps[ref] != NULL) {
                    metadata = xfsm->table->curr_pkt->userdata;
                    memset(metadata, 0, sizeof(pkt_metadata_t));
                    xfsm->table->curr_pkt = rte_pktmbuf_clone(xfsm->table->pkttmps[ref], xfsm->table->pkttmp_pool);
                    xfsm->table->curr_pkt->userdata = metadata;
                    parse_pkt_header(xfsm->table, xfsm->table->curr_pkt);
                } else
                    rte_exit(EXIT_FAILURE, "No configured pkttmp\n");
            }
            default:
                break;
        }
    }
    return 0;
}
*/


int
xfsm_execute_actions_outport(xfsm_table_entry_t *match, xfsm_instance_t *xfsm, uint16_t *out) {
    int64_t reg1 = 1, reg2 = 1, reg3 = 1;
    int i;
    int out_pkts = 0;

    RTE_LOG(INFO, XFSM, "Entering execute actions\n");

    /*for (i=0; i<NUM_REGISTERS/sizeof(uint64_t); i+=CACHE_LINE_SIZE) {
        rte_prefetch0(xfsm->registers + i);
        rte_prefetch0(xfsm->table->global_registers+i);
    }*/

    for (i=0; i<match->nb_actions; i++){
/*
        rte_prefetch0(&match->actions[i]);
*/
        xfsm_action_t *act = &match->actions[i];

        RTE_LOG(INFO, XFSM, "Processing action %d: type=%s\n", i, xfsm_action_names[act->type]);

        switch (act->type1){
            case XFSM_REGISTER:
                reg1 = xfsm->registers[act->value1];
                break;
            case XFSM_GLOBAL_REGISTER:
                reg1 = xfsm->table->global_registers[act->value1];
                break;
            case XFSM_CONSTANT:
                reg1 = act->value1;
                break;
            case XFSM_HDR_FIELD:
                if (act->type == XFSM_SETFIELD)
                    reg1 = act->value1;
                else {
                    if (get_hdr_value(xfsm->table->pkt_fields[act->value1], (uint64_t *) &reg1) < 0)
                        RTE_LOG(ERR, XFSM, "Packet field not found\n");
                }
                break;
            default:
                if (act->type == XFSM_DESTROY)
                    break;
                RTE_LOG(INFO, XFSM, "Invalid field 1 type\n");
                break;
        }

        switch (act->type2){
            case XFSM_REGISTER:
                reg2 = xfsm->registers[act->value2];
                break;
            case XFSM_GLOBAL_REGISTER:
                reg2 = xfsm->table->global_registers[act->value2];
                break;
            case XFSM_CONSTANT:
                reg2 = act->value2;
                break;
            case XFSM_HDR_FIELD:
                if (get_hdr_value(xfsm->table->pkt_fields[act->value2], (uint64_t *) &reg2) < 0)
                    RTE_LOG(ERR, XFSM, "Packet field not found\n");
                break;
            default:
                if (act->type == XFSM_DESTROY)
                    break;
                RTE_LOG(INFO, XFSM, "Invalid field 2 type\n");
                break;
        }

        switch (act->type3){
            case XFSM_REGISTER:
                reg3 = xfsm->registers[act->value3];
                break;
            case XFSM_GLOBAL_REGISTER:
                reg3 = xfsm->table->global_registers[act->value3];
                break;
            case XFSM_CONSTANT:
                reg3 = act->value3;
                break;
            case XFSM_HDR_FIELD:
                if (get_hdr_value(xfsm->table->pkt_fields[act->value3], (uint64_t *) &reg3) < 0)
                    RTE_LOG(ERR, XFSM, "Packet field not found\n");
                break;
            default:
                if (act->type == XFSM_DESTROY)
                    break;
                RTE_LOG(INFO, XFSM, "Invalid field 3 type\n");
                break;
        }

        //RTE_LOG(INFO, XFSM, "\nreg1: %lu, reg2: %lu, reg3: %lu\n", reg1, reg2, reg3);

        switch(act->type) {
            case XFSM_UPDATE:
            {
                update(reg1, reg2, act->opcode, act->output, act->out_type,
                       xfsm->registers, xfsm->table->global_registers);
                break;
            }
                //                reg1    const|reg2
                // send_packet(packet_ref, out_port);
            case XFSM_SENDPKT:
            {
                //reg2 = act->value2;

                if (unlikely(reg2 == 0)) {
                    RTE_LOG(ERR, XFSM, "out port is 0|\n");
                    break;
                }

                xfsm->table->curr_pkt->l2_len = ETHER_HDR_LEN;
                xfsm->table->curr_pkt->l3_len = IPv4_HDR_LEN;
                xfsm->table->curr_pkt->userdata = NULL;

                // set ipv4 checksum to 0
                memset(xfsm->table->pkt_fields[IPv4_TOS].value + 9, 0, 2);
                xfsm->table->curr_pkt->ol_flags |= PKT_TX_IPV4 | PKT_TX_IP_CKSUM;

                SET(*out, (reg2-1));
                RTE_LOG(INFO, XFSM, "out_port: %d\n", (int) reg2-1);
                out_pkts++;
                break;
            }
                // set_field(field_name, pkt_ref, value)
                //              reg1      const    reg3
            case XFSM_SETFIELD:
            {
                uint64_t field_name, value;

                field_name = (uint64_t) reg1;
                value = (uint64_t) reg3;

                switch (xfsm->table->pkt_fields[field_name].field->len){
                    case 8:
                        value = (uint64_t) reg3;
                        break;
                    case 6:
                        value <<= 16;
                        value = rte_cpu_to_be_64(value);
                        break;
                    case 4:
                        value = rte_cpu_to_be_32( (uint32_t) value);
                        break;
                    case 2:
                        value = rte_cpu_to_be_16( (uint16_t) value);
                        break;
                    default:
                        break;
                }

                if (xfsm->table->pkt_fields[field_name].value != NULL) {
                    // value of packet fields are pointers to the packet field
                    // this memcpy copies the value directly in the packet header

                    rte_memcpy(xfsm->table->pkt_fields[field_name].value, &value,
                               xfsm->table->pkt_fields[field_name].field->len);

                    RTE_LOG(INFO, XFSM, "Executing set field: field: %s, value: %lu\n",
                            xfsm->table->pkt_fields[field_name].field->name, value);
                } else
                    RTE_LOG(INFO, XFSM, "Field not found in packet header... doing nothing\n");

                break;
            }
            case XFSM_DESTROY:
            {
                // good bye my darling
                xfsm->destroy = true;
                break;
            }
            case XFSM_ENCAP_GTP:
            {
                uint8_t *pkt_data = NULL;
                uint8_t off = 0;
                uint8_t encap_size = IPv4_HDR_LEN + UDP_HDR_LEN + GTP_HDR_LEN;

                if (rte_pktmbuf_headroom(xfsm->table->curr_pkt) >= encap_size) {
                    pkt_data = (uint8_t *) rte_pktmbuf_prepend(xfsm->table->curr_pkt, encap_size);

                    rte_memcpy(pkt_data-(IPv4_HDR_LEN+UDP_HDR_LEN+GTP_HDR_LEN), pkt_data, ETHER_HDR_LEN);
                } else if (rte_pktmbuf_tailroom(xfsm->table->curr_pkt) >= IPv4_HDR_LEN + UDP_HDR_LEN + GTP_HDR_LEN) {
                    rte_pktmbuf_append(xfsm->table->curr_pkt, encap_size);

                    rte_memcpy(pkt_data + (ETHER_HDR_LEN + encap_size),
                               pkt_data + ETHER_HDR_LEN,
                               (size_t ) rte_pktmbuf_data_len(xfsm->table->curr_pkt) - ETHER_HDR_LEN);
                } else {
                    rte_exit(EXIT_FAILURE, "Error: no space left on pktmbuf structure\n");
                }

                // populate the outer headers
                // IPv4
                pkt_data = rte_pktmbuf_mtod(xfsm->table->curr_pkt, uint8_t *);
                off += ETHER_HDR_LEN;
                struct ipv4_hdr *oip = (struct ipv4_hdr *) (pkt_data + off);
                oip->version_ihl = 0x45;
                oip->type_of_service = 0;
                oip->fragment_offset = 0;
                oip->hdr_checksum = 0;
                oip->packet_id = 1;
                oip->next_proto_id = 0x11;
                oip->time_to_live = 64;
                oip->total_length = (uint16_t ) rte_cpu_to_be_16(rte_pktmbuf_data_len(xfsm->table->curr_pkt)
                                                                 - ETHER_HDR_LEN);
                oip->dst_addr = 0xaabbccdd;
                oip->src_addr = 0xddccbbaa;

                off += IPv4_HDR_LEN;
                struct udp_hdr *udp = (struct udp_hdr *) (pkt_data + off);
                udp->src_port = rte_cpu_to_be_16(54545);
                udp->dst_port = rte_cpu_to_be_16(GTP_UDP_PORT);
                udp->dgram_len = (uint16_t) rte_cpu_to_be_16(rte_pktmbuf_data_len(xfsm->table->curr_pkt)
                                                             - ETHER_HDR_LEN - IPv4_HDR_LEN);
                udp->dgram_cksum = 0;

                off += UDP_HDR_LEN;
                struct gtp_u_header *gtp = (struct gtp_u_header *) (pkt_data + off);
                gtp->flags = 0x30;
                gtp->msg_type = 0xff;
                gtp->len = (uint16_t ) rte_cpu_to_be_16(rte_pktmbuf_data_len(xfsm->table->curr_pkt)
                                                        - ETHER_HDR_LEN - IPv4_HDR_LEN - UDP_HDR_LEN - GTP_HDR_LEN);
                gtp->teid = 0x10101010;

                parse_pkt_header(xfsm->table, xfsm->table->curr_pkt);

                break;
            }
            case XFSM_DECAP_GTP:
            {
                uint8_t *pkt_data = rte_pktmbuf_mtod(xfsm->table->curr_pkt, uint8_t *);

                rte_memcpy(pkt_data + IPv4_HDR_LEN + UDP_HDR_LEN + GTP_HDR_LEN, pkt_data, ETHER_HDR_LEN);
                rte_pktmbuf_adj(xfsm->table->curr_pkt, IPv4_HDR_LEN + UDP_HDR_LEN + GTP_HDR_LEN);
                break;
            }
            case XFMS_GENPKT:
            {
                uint8_t ref = (uint8_t) reg1;
                pkt_metadata_t *metadata;
                struct rte_mbuf *pkt;

                if (xfsm->table->pkttmps[ref] != NULL) {
                    metadata = (pkt_metadata_t *) xfsm->table->curr_pkt->userdata;

                    memset(metadata, 0, sizeof(pkt_metadata_t));
                    pkt = rte_pktmbuf_clone(xfsm->table->pkttmps[ref], xfsm->table->pkttmp_pool);

                    rte_pktmbuf_free(xfsm->table->curr_pkt);

                    xfsm->table->curr_pkt = pkt;
                    xfsm->table->curr_pkt->userdata = metadata;
                    parse_pkt_header(xfsm->table, xfsm->table->curr_pkt);
                } else
                    rte_exit(EXIT_FAILURE, "No configured pkttmp\n");
            }
            default:
                break;
        }
    }

    RTE_LOG(INFO, XFSM, "Next state is %d\n", match->next_state);
    xfsm->current_state = match->next_state;
    return out_pkts;
}

int
xfsm_execute_actions(xfsm_table_entry_t *match, xfsm_instance_t *xfsm, struct rte_eth_dev_tx_buffer *tx_buffer,
                     uint16_t queue_id) {
    int64_t reg1 = 1, reg2 = 1, reg3 = 1;
    int i;

    RTE_LOG(INFO, XFSM, "Entering execute actions\n");

    /*for (i=0; i<NUM_REGISTERS/sizeof(uint64_t); i+=CACHE_LINE_SIZE) {
        rte_prefetch0(xfsm->registers + i);
        rte_prefetch0(xfsm->table->global_registers+i);
    }
*/
    for (i=0; i<match->nb_actions; i++){

        //rte_prefetch0(&match->actions[i+1]);
        xfsm_action_t *act = &match->actions[i];

        RTE_LOG(INFO, XFSM, "Processing action %d: type=%s\n", i, xfsm_action_names[act->type]);

        switch (act->type1){
            case XFSM_REGISTER:
                reg1 = xfsm->registers[act->value1];
                break;
            case XFSM_GLOBAL_REGISTER:
                reg1 = xfsm->table->global_registers[act->value1];
                break;
            case XFSM_CONSTANT:
                reg1 = act->value1;
                break;
            case XFSM_HDR_FIELD:
                if (act->type == XFSM_SETFIELD)
                    reg1 = act->value1;
                else {
                    if (get_hdr_value(xfsm->table->pkt_fields[act->value1], (uint64_t *) &reg1) < 0)
                        RTE_LOG(ERR, XFSM, "Packet field not found\n");
                }
                break;
            default:
                if (act->type == XFSM_DESTROY)
                    break;
                RTE_LOG(INFO, XFSM, "Invalid field 1 type\n");
                break;
        }

        switch (act->type2){
            case XFSM_REGISTER:
                reg2 = xfsm->registers[act->value2];
                break;
            case XFSM_GLOBAL_REGISTER:
                reg2 = xfsm->table->global_registers[act->value2];
                break;
            case XFSM_CONSTANT:
                reg2 = act->value2;
                break;
            case XFSM_HDR_FIELD:
                if (get_hdr_value(xfsm->table->pkt_fields[act->value2], (uint64_t *) &reg2) < 0)
                    RTE_LOG(ERR, XFSM, "Packet field not found\n");
                break;
            default:
                if (act->type == XFSM_DESTROY)
                    break;
                RTE_LOG(INFO, XFSM, "Invalid field 2 type\n");
                break;
        }

        switch (act->type3){
            case XFSM_REGISTER:
                reg3 = xfsm->registers[act->value3];
                break;
            case XFSM_GLOBAL_REGISTER:
                reg3 = xfsm->table->global_registers[act->value3];
                break;
            case XFSM_CONSTANT:
                reg3 = act->value3;
                break;
            case XFSM_HDR_FIELD:
                if (get_hdr_value(xfsm->table->pkt_fields[act->value3], (uint64_t *) &reg3) < 0)
                    RTE_LOG(ERR, XFSM, "Packet field not found\n");
                break;
            default:
                if (act->type == XFSM_DESTROY)
                    break;
                RTE_LOG(INFO, XFSM, "Invalid field 3 type\n");
                break;
        }


        //RTE_LOG(INFO, XFSM, "\nreg1: %lu, reg2: %lu, reg3: %lu\n", reg1, reg2, reg3);

        switch(act->type) {
            case XFSM_UPDATE:
            {
                update(reg1, reg2, act->opcode, act->output, act->out_type,
                       xfsm->registers, xfsm->table->global_registers);
                break;
            }
                //                reg1    const|reg2
                // send_packet(packet_ref, out_port);
            case XFSM_SENDPKT:
            {
                reg2 = act->value2;

                int ret = 0;
                if (unlikely(reg2 == 0)) {
                    RTE_LOG(ERR, XFSM, "out port is 0|\n");
                    break;
                }

                xfsm->table->curr_pkt->l2_len = ETHER_HDR_LEN;
                xfsm->table->curr_pkt->l3_len = IPv4_HDR_LEN;
                xfsm->table->curr_pkt->userdata = NULL;

                // set ipv4 checksum to 0
                memset(xfsm->table->pkt_fields[IPv4_TOS].value + 9, 0, 2);
                xfsm->table->curr_pkt->ol_flags |= PKT_TX_IPV4 | PKT_TX_IP_CKSUM;

                ret = rte_eth_tx_buffer((uint16_t) (reg2-1), queue_id, &tx_buffer[reg2-1], xfsm->table->curr_pkt);
                RTE_LOG(INFO, XFSM, "out_port: %d, return code: %d\n", (int) reg2-1, ret);
                break;
            }
                // set_field(field_name, pkt_ref, value)
                //              reg1      const    reg3
            case XFSM_SETFIELD:
            {
                uint64_t field_name, value;

                field_name = (uint64_t) reg1;
                value = (uint64_t) reg3;

                switch (xfsm->table->pkt_fields[field_name].field->len){
                    case 8:
                        value = (uint64_t) reg3;
                        break;
                    case 6:
                        value <<= 16;
                        value = rte_cpu_to_be_64(value);
                        break;
                    case 4:
                        value = rte_cpu_to_be_32( (uint32_t) value);
                        break;
                    case 2:
                        value = rte_cpu_to_be_16( (uint16_t) value);
                        break;
                    default:
                        break;
                }

                if (xfsm->table->pkt_fields[field_name].value != NULL) {
                    // value of packet fields are pointers to the packet field
                    // this memcpy copies the value directly in the packet header

                    rte_memcpy(xfsm->table->pkt_fields[field_name].value, &value,
                               xfsm->table->pkt_fields[field_name].field->len);

                    RTE_LOG(INFO, XFSM, "Executing set field: field: %s, value: %lu\n",
                            xfsm->table->pkt_fields[field_name].field->name, value);
                } else
                    RTE_LOG(INFO, XFSM, "Field %s not found in packet header... doing nothing\n",
                            xfsm->table->pkt_fields[field_name].field->name);

                break;
            }
            case XFSM_DESTROY:
            {
                // good bye my darling
                xfsm->destroy = true;
                break;
            }
            case XFSM_ENCAP_GTP:
            {
               /* uint8_t *pkt_data = rte_pktmbuf_mtod(xfsm->table->curr_pkt, uint8_t *);
                uint8_t off = 0;
                if (rte_pktmbuf_headroom(xfsm->table->curr_pkt) >= IPv4_HDR_LEN + UDP_HDR_LEN + GTP_HDR_LEN) {
                    rte_memcpy(pkt_data-(IPv4_HDR_LEN+UDP_HDR_LEN+GTP_HDR_LEN), pkt_data, ETHER_HDR_LEN);

                    xfsm->table->curr_pkt->data_len += IPv4_HDR_LEN + UDP_HDR_LEN + GTP_HDR_LEN;
                    xfsm->table->curr_pkt->data_off -= IPv4_HDR_LEN + UDP_HDR_LEN + GTP_HDR_LEN;
                } else if (rte_pktmbuf_tailroom(xfsm->table->curr_pkt) >= IPv4_HDR_LEN + UDP_HDR_LEN + GTP_HDR_LEN) {
                    rte_memcpy(pkt_data + (ETHER_HDR_LEN+IPv4_HDR_LEN+UDP_HDR_LEN+GTP_HDR_LEN),
                               pkt_data + ETHER_HDR_LEN,
                               (size_t ) rte_pktmbuf_data_len(xfsm->table->curr_pkt) - ETHER_HDR_LEN);

                    xfsm->table->curr_pkt->data_len += IPv4_HDR_LEN + UDP_HDR_LEN + GTP_HDR_LEN;
                } else {
                    rte_exit(EXIT_FAILURE, "Error: no space left on pktmbuf structure\n");
                }

                // populate the outer headers
                // IPv4
                pkt_data = rte_pktmbuf_mtod(xfsm->table->curr_pkt, uint8_t *);
                off += ETHER_HDR_LEN;
                struct ipv4_hdr *oip = (struct ipv4_hdr *) (pkt_data + off);
                oip->version_ihl = 0x45;
                oip->type_of_service = 0;
                oip->fragment_offset = 0;
                oip->hdr_checksum = 0;
                oip->packet_id = 1;
                oip->next_proto_id = 0x11;
                oip->time_to_live = 64;
                oip->total_length = (uint16_t ) rte_cpu_to_be_16(rte_pktmbuf_data_len(xfsm->table->curr_pkt)
                                                                 - ETHER_HDR_LEN);
                oip->dst_addr = 0xaabbccdd;
                oip->src_addr = 0xddccbbaa;

                off += IPv4_HDR_LEN;
                struct udp_hdr *udp = (struct udp_hdr *) (pkt_data + off);
                udp->src_port = rte_cpu_to_be_16(54545);
                udp->dst_port = rte_cpu_to_be_16(GTP_UDP_PORT);
                udp->dgram_len = (uint16_t) rte_cpu_to_be_16(rte_pktmbuf_data_len(xfsm->table->curr_pkt)
                                                             - ETHER_HDR_LEN - IPv4_HDR_LEN);
                udp->dgram_cksum = 0;

                off += UDP_HDR_LEN;
                struct gtp_u_header *gtp = (struct gtp_u_header *) (pkt_data + off);
                gtp->flags = 0x30;
                gtp->msg_type = 0xff;
                gtp->len = (uint16_t ) rte_cpu_to_be_16(rte_pktmbuf_data_len(xfsm->table->curr_pkt)
                                                        - ETHER_HDR_LEN - IPv4_HDR_LEN - UDP_HDR_LEN - GTP_HDR_LEN);
                gtp->teid = 0x10101010;

                parse_pkt_header(xfsm->table, xfsm->table->curr_pkt);
*/
                break;
            }
            case XFSM_DECAP_GTP:
            {
                uint8_t *pkt_data = rte_pktmbuf_mtod(xfsm->table->curr_pkt, uint8_t *);

                rte_memcpy(pkt_data + IPv4_HDR_LEN + UDP_HDR_LEN + GTP_HDR_LEN, pkt_data, ETHER_HDR_LEN);
                xfsm->table->curr_pkt->data_off += IPv4_HDR_LEN + UDP_HDR_LEN + GTP_HDR_LEN;
                xfsm->table->curr_pkt->data_len -= IPv4_HDR_LEN + UDP_HDR_LEN + GTP_HDR_LEN;

                break;
            }
            case XFMS_GENPKT:
            {
                uint8_t ref = (uint8_t) reg1;
                pkt_metadata_t *metadata;

                if (xfsm->table->pkttmps[ref] != NULL) {
                    metadata = xfsm->table->curr_pkt->userdata;
                    memset(metadata, 0, sizeof(pkt_metadata_t));
                    xfsm->table->curr_pkt = rte_pktmbuf_clone(xfsm->table->pkttmps[ref], xfsm->table->pkttmp_pool);
                    xfsm->table->curr_pkt->userdata = metadata;
                    parse_pkt_header(xfsm->table, xfsm->table->curr_pkt);
                } else
                    rte_exit(EXIT_FAILURE, "No configured pkttmp\n");
            }
            default:
                break;
        }
    }
    return 0;
}

void dump_tables(xfsm_table_t *xfsm, int stage, FILE *file){
    xfsm_instance_t *instance = NULL;

    fprintf(file, "\n################## STAGE: %d\n", stage);
    fprintf(file, "Global registers: [%"PRIi64",%"PRIi64",%"PRIi64",%"PRIi64"]\n",
            xfsm->global_registers[0], xfsm->global_registers[1], xfsm->global_registers[2], xfsm->global_registers[3]);

    for (uint i=0; i<xfsm->nb_instances; i++) {
        instance = &xfsm->instances_pool[i];

        fprintf(file, "Instance number %d. Printing registers...\n", i);
        for (uint j=0; j<4; j++) {
            fprintf(file, "\treg%d: %"PRIi64"\n", j, instance->registers[j]);
        }
    }

}

static inline void
biflow(uint32_t *key, lookup_key_t *lookup_flds) {
    uint32_t tmp;

    RTE_LOG(INFO, XFSM, "Biflow handling...\n");

    for (int i=0; i<lookup_flds->biflow_no * 2; i+=2){
        if (key[i] > key[i+1]) {
            tmp = key[i];
            key[i] = key[i+1];
            key[i+1] = tmp;
            RTE_LOG(INFO, XFSM, "Swapped fields\n");
        }
    }
}

bool
key_extract_outside(xfsm_table_t *xfsm, xfsm_fields_tlv_t *pkt_fields, flow_key_t *key) {
    hdr_fields fld;

    for (int i=0; i<xfsm->lookup_key.size; i++){
        fld = xfsm->lookup_key.fld[i];
        if (get_hdr_value(pkt_fields[fld], (uint64_t *) &key->flds[i]) < 0)
            RTE_LOG(ERR, XFSM, "Packet field not found: %s\n", pkt_fields[fld].field->name);
        else
            RTE_LOG(INFO, XFSM, "\tfield %d: %u\n", i, key->flds[i]);
    }

    if (xfsm->lookup_key.biflow_no > 0)
        biflow(key->flds, &xfsm->lookup_key);

    //RTE_LOG(INFO, XFSM, "Key extracted\n");
    return true;
}

bool
key_extract(xfsm_table_t *xfsm, flow_key_t *key) {
    hdr_fields fld;

    for (int i=0; i<xfsm->lookup_key.size; i++){
        fld = xfsm->lookup_key.fld[i];
        if (get_hdr_value(xfsm->pkt_fields[fld], (uint64_t *) &key->flds[i]) < 0) {
            RTE_LOG(ERR, XFSM, "Packet field not found: %s\n", xfsm->pkt_fields[fld].field->name);
            return false;
        }
        else
            RTE_LOG(INFO, XFSM, "\tfield %d: %u\n", i, key->flds[i]);
    }

    if (xfsm->lookup_key.biflow_no > 0)
        biflow(key->flds, &xfsm->lookup_key);

    //RTE_LOG(INFO, XFSM, "Key extracted\n");
    return true;
}
